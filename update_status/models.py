from django.db import models
from django.utils import timezone

class Todo(models.Model):
    fill = models.CharField(max_length=1000)
    created_date = models.DateTimeField(auto_now_add=True)
