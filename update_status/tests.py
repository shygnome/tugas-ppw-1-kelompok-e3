from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, update_status_todo, remove_todo
from .models import Todo
from .forms import Todo_Form

# Create your tests here.
class Lab5UnitTest(TestCase):

    def test_update_status_url_is_exist(self):
        response = Client().get('/update-status/')
        self.assertEqual(response.status_code, 200)
		
    def test_root_url_now_is_using_index_page_from_update_status(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 301)
        self.assertRedirects(response,'/update-status/',301,200)

    def test_update_status_using_index_func(self):
        found = resolve('/update-status/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_todo(self):
        # Creating a new activity
        new_activity = Todo.objects.create(fill='mengerjakan lab ppw')

        # Retrieving all available activity
        counting_all_available_todo = Todo.objects.all().count()
        self.assertEqual(counting_all_available_todo, 1)
		
    def test_form_validation_for_blank_items(self):
        form = Todo_Form(data={'fill': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['fill'],
            ["This field is required."]
        )
    def test_update_status_post_success_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/update-status/update_status_todo', {'fill': test})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/update-status/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_update_status_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/update-status/update_status_todo', {'fill': ''})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/update-status/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)
		
    def test_update_status_delete_post(self):
        new_todo = Todo.objects.create(fill = "internet")
        object = Todo.objects.all()[0]
        response_post = Client().post('/update-status/remove_todo', {'flag': str(object.id)})

        response= Client().get('/update-status/')
        html_response = response.content.decode('utf8')
        self.assertNotIn('<form id="form-' + str(object.id) + '"', html_response)
		
    def test_update_status_error_flag(self):
        new_todo = Todo.objects.create(fill = "ccd internet")
        object = Todo.objects.all()[0]
        response_post = Client().post('/update-status/remove_todo', {'flag': "WIBUWUUWBWBWDBDUWDU"})

        response= Client().get('/update-status/')
        html_response = response.content.decode('utf8')
        self.assertIn('<form id="form-' + str(object.id) + '"', html_response)