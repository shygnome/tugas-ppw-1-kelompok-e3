from django import forms

class Todo_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
    }
    fill_attrs = {
        'type': 'text',
        'cols': 151,
        'rows': 3,
        'class': 'todo-form-textarea',
        'placeholder':'Apa Aktivitas Anda Saat Ini?'
    }

    fill = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=fill_attrs))



