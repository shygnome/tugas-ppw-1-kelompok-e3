from django.shortcuts import render
from add_friend.models import Friend
from update_status.models import Todo
# Create your views here.

def index(request):
	friend = Friend.objects.count()
	feed = Todo.objects.count()
	lastFeed = Todo.objects.last()
	#lastFeed = Todo.objects.all()[Todo.objects.count()-1]
	response = {'author' : 'Hepzibah Smith', 'friend' : friend, 'feed' : feed, 'lastfeed' : lastFeed}
	return render(request, 'stats.html', response)
