from django.db import models

# Create your models here.
class Biodata(models.Model):
	name = models.CharField(max_length = 100)
	birthday = models.DateField(max_length = 100)
	gender = models.CharField(max_length = 100)
	expertise = models.CharField(max_length = 100)
	description = models.TextField(max_length = 100)
	email = models.EmailField(max_length = 100)
	