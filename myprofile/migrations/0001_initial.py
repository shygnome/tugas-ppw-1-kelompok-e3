# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-10-12 20:18
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Biodata',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('birthday', models.DateField(max_length=100)),
                ('gender', models.CharField(max_length=100)),
                ('expertise', models.CharField(max_length=100)),
                ('description', models.TextField(max_length=100)),
                ('email', models.EmailField(max_length=100)),
            ],
        ),
    ]
