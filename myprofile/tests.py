from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index
from .models import Biodata

# Create your tests here.
class MyProfileUnitTest(TestCase):
	def test_my_profile_url_is_exist(self):
		new_biodata = Biodata.objects.create(name='Tom Riddle',birthday='1998-01-01',gender='Female',expertise='Marketing, Collector, Public-Speaking',description='Antique expert. Experience as marketers for 10 years',email='hello@smith.com')
		
		#Retrieving all available biodata object
		counting_all_available_biodata = Biodata.objects.all().count()
		self.assertEqual(counting_all_available_biodata, 1)
		
		response = Client().get('/myprofile/')
		self.assertEqual(response.status_code, 200)

	def test_using_index_func(self):
		found = resolve('/myprofile/')
		self.assertEqual(found.func, index)
	
	