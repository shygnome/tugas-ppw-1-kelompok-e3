from django import forms

class Add_Friend_Form(forms.Form):
	error_messages = {
		'required': 'Input ini harus diisi',
		'invalid': 'Isi input dengan url',
	}
	attrs = {
		'class': 'form-control',
	}
	name_attrs = {
		'type': 'text',
        'class': 'form-control',
        'placeholder':'Enter your friend\'s name'
	}
	url_attrs = {
		'type': 'text',
        'class': 'form-control',
        'placeholder':'Enter your friend\'s url'
	}
	
	name = forms.CharField(label='Name', required=True, max_length=30, widget=forms.TextInput(attrs=name_attrs), error_messages=error_messages)
	url = forms.URLField(label='URL', required=True, widget=forms.URLInput(attrs=url_attrs), error_messages=error_messages)