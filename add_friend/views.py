from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django import forms
from .forms import Add_Friend_Form
from .models import Friend

# Create your views here.
response = {}
html = 'add_friend.html'

def index(request):
	friend = Friend.objects.all()
	response['author'] = 'Kelompok 3 PPW-E'
	response['friend'] = friend
	response['friend_form'] = Add_Friend_Form
	return render(request, html, response)
	
def add_friend(request):
	form = Add_Friend_Form(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		friend = Friend(name = request.POST['name'], url = request.POST['url'])
		friend.save()
	return HttpResponseRedirect('/friends/')