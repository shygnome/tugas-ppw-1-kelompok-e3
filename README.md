# Tugas 1 PPW Kelas E Kelompok 3

Tugas 1 PPW Kelas E Kelompok 3

[![Build status](https://gitlab.com/shygnome/tugas-ppw-1-kelompok-e3/badges/master/pipeline.svg)](https://gitlab.com/shygnome/tugas-ppw-1-kelompok-e3/commits/master)
[![Overall test coverage](https://gitlab.com/shygnome/tugas-ppw-1-kelompok-e3/badges/master/coverage.svg)](https://gitlab.com/shygnome/tugas-ppw-1-kelompok-e3/pipelines)

## Nama Anggota Kelompok

* Muhammad Yudistira Hanifmuti
* Priambudi Lintang Bagaskara
* Ryan Naufal Pioscha
* Faizah Afifah

### Link Herokuapp

"""
https://kelompok-3-tugas-1-ppw-e.herokuapp.com/
"""

